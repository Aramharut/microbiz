class Main {
    textRates = [7, 20, 0];
    roundingRoles = ['95', '99', '00'];
    pricingModes = ['Tax Inclusive Prices', 'Tax Exclusive Prices'];
    roundingRole = this.roundingRoles[0];
    amount = 0;
    cost = 0;
    defaultMarkup = 0;
    taxRate;

    taxExclusivePrice = 0;

    constructor() {
        this.drawTextRateOptions();
        this.drawRoundingRoleOptions();
        this.drawPricingModeOptions();
        this.calculateBtnListener();
        this.inputListners()
    }

    inputListners() {
        document.getElementById("defaultMarkup").addEventListener('input', () => {
            if (document.getElementById("defaultMarkup").value) {
                document.getElementById("errMarkup").className = 'err-markup disabled';
                if (document.getElementById("cost").value) {
                    document.getElementById("calculate").removeAttribute('disabled');
                }
            } else {
                document.getElementById("errMarkup").className = 'err-markup';
                document.getElementById("calculate").setAttribute('disabled', 'true');
            }
        });

        document.getElementById("cost").addEventListener('input', () => {
            if (document.getElementById("cost").value) {
                document.getElementById("errCoast").className = 'err-cost disabled';
                if (document.getElementById("defaultMarkup").value) {
                    document.getElementById("calculate").removeAttribute('disabled');
                }
            } else {
                document.getElementById("errCoast").className = 'err-cost';
                document.getElementById("calculate").setAttribute('disabled', 'true');
            }
        });
    }

    initValues() {
        this.defaultMarkup = document.getElementById("defaultMarkup").value;
        this.cost = document.getElementById("cost").value;
        this.roundingRole = document.getElementById("roundingRole").value;
        this.taxRate = parseInt(document.getElementById("taxRate").value);
    }

    calculateAmount() {
        this.amount = this.taxExclusivePrice = this.cost / Math.abs(1 - this.defaultMarkup / 100);
    }

    setTaxRate() {
        this.amount = this.taxRate ? this.amount + (this.amount * this.taxRate / 100) : this.amount;
    }

    setRoundingRole() {
        if (this.roundingRole === '99') {
            this.amount = Math.round(this.amount) - 0.01;
        } else if (this.roundingRole === '00' && Math.ceil(this.amount) - this.amount < 0.6) {
            this.amount = Math.ceil(this.amount);
        } else if (this.roundingRole === '95' && Math.ceil(this.amount) - this.amount < 0.55) {
            this.amount = Math.ceil(this.amount) - 0.05;
        }
    }

    calculateBtnListener() {
        document.getElementById("calculate").addEventListener("click", () => {
            this.initValues();
            this.calculateAmount();
            this.setRoundingRole();
            this.setTaxRate();
            this.drawValues();
        });
    }

    drawValues() {
        document.getElementById("taxInclusivePrice").innerHTML = this.amount;
        document.getElementById("grossProfitAmount").innerHTML = this.amount - this.cost;
        document.getElementById("grossProfitPersent").innerHTML = this.cost / this.amount * 100.;
        document.getElementById("taxExclusivePrice").innerHTML = this.taxExclusivePrice;
    }

    drawTextRateOptions() {
        this.textRates.forEach(option => {
            let node = document.createElement("option");
            node.appendChild(document.createTextNode(option + '%'));
            document.getElementById("taxRate").appendChild(node);
        })
    }

    drawRoundingRoleOptions() {
        this.roundingRoles.forEach(option => {
            let node = document.createElement("option");
            node.appendChild(document.createTextNode(option));
            document.getElementById("roundingRole").appendChild(node);
        })
    }

    drawPricingModeOptions() {
        this.pricingModes.forEach(option => {
            let node = document.createElement("option");
            node.appendChild(document.createTextNode(option));
            document.getElementById("pricingMode").appendChild(node);
        })
    }
}

